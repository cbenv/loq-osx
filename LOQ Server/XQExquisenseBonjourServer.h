//
//  XQExquisenseBonjourServer.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/22/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol XQExquisenseBonjourServerDelegate <NSObject>

@optional

- (void)didReceiveData:(NSData *)data;
- (void)didDisconnect;
- (void)didConnect;

@end

@interface XQExquisenseBonjourServer : NSObject

@property (weak, nonatomic) id <XQExquisenseBonjourServerDelegate> delegate;

- (id)init;
- (id)initWithServiceType:(NSString *)type;
- (id)initWithServiceType:(NSString *)type inDomain:(NSString *)domain;
- (id)initWithServiceType:(NSString *)type name:(NSString *)name inDomain:(NSString *)domain;
- (void)startBroadcasting;
- (void)stop;

- (NSString *)getPairedDeviceName;

- (void)sendData:(NSData *)data;

@end
