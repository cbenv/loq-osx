//
//  XQAppDelegate.m
//  LOQ Server
//
//  Created by Christoforus Juan Benvenuto on 7/20/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQAppDelegate.h"
#import "XQExquisenseBonjourServer.h"

#define ENABLE_LOGGING YES

#define SERVICE_NAME @"_clipboard._tcp."

@interface XQAppDelegate () <XQExquisenseBonjourServerDelegate>
{
    NSStatusItem *statusItem;
    NSMenuItem *statusMenuItem;
    NSMenuItem *clipboardStatus;
    NSMenuItem *quitMenuItem;
    
    XQExquisenseBonjourServer *server;
    
    NSString *pairedDevice;
}

@property (weak) IBOutlet NSMenu *statusMenu;

@end

@implementation XQAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    
}

- (void)awakeFromNib
{
    statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    [statusItem setMenu:self.statusMenu];
    [statusItem setTitle:@"LOQ"];
    [statusItem setHighlightMode:YES];
    
    statusMenuItem = [[NSMenuItem alloc] initWithTitle:@"Disconnected" action:NULL keyEquivalent:@""];
    quitMenuItem = [[NSMenuItem alloc] initWithTitle:@"Quit" action:@selector(quitMenuItemSelected) keyEquivalent:@"q"];
    
    [self.statusMenu addItem:statusMenuItem];
    [self.statusMenu addItem:quitMenuItem];
    
    server = [[XQExquisenseBonjourServer alloc] initWithServiceType:@"clipboard"];
    [server setDelegate:self];
    [server startBroadcasting];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    [server stop];
}

#pragma mark - Selector Methods

- (void)quitMenuItemSelected
{
    [NSApp terminate:self];
}

#pragma mark - XQExquisenseBonjourServerDelegate

- (void)didReceiveData:(NSData *)data
{
    NSString *message = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    [[NSPasteboard generalPasteboard] clearContents];
    [[NSPasteboard generalPasteboard] writeObjects:[NSArray arrayWithObject:message]];
}

- (void)didConnect
{
    [statusMenuItem setTitle:@"Connected"];
}

- (void)didDisconnect
{
    [statusMenuItem setTitle:@"Disconnected"];
}

@end
