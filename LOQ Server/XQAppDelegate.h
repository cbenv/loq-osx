//
//  XQAppDelegate.h
//  LOQ Server
//
//  Created by Christoforus Juan Benvenuto on 7/20/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface XQAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
