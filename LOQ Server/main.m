//
//  main.m
//  LOQ Server
//
//  Created by Christoforus Juan Benvenuto on 7/20/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
