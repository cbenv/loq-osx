//
//  XQExquisenseBonjourServer.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/22/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseBonjourServer.h"

#import <sys/socket.h>
#import <netinet/in.h>
#import <unistd.h>
#import <CFNetwork/CFSocketStream.h>

@interface XQExquisenseBonjourServer () <NSNetServiceDelegate, NSStreamDelegate>
{
    NSNetService *netService; // announcing service
    NSNetService *listeningService;
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    
    NSString *netServiceType;
    NSString *netServiceTransportLayer;
    NSString *netServiceDomain;
    NSString *netServiceName;
    uint16_t netServicePort;
    uint8_t netServicePayloadSize;
    
    CFSocketRef socket;
}

@end

@implementation XQExquisenseBonjourServer

- (id)init
{
    self = [super init];
    if (self)
    {
        self = [self initWithServiceType:@"server"];
    }
    return self;
}

- (id)initWithServiceType:(NSString *)type
{
    self = [super init];
    if (self)
    {
        self = [self initWithServiceType:type inDomain:@""];
    }
    return self;
}

- (id)initWithServiceType:(NSString *)type inDomain:(NSString *)domain
{
    self = [super init];
    if (self)
    {
        self = [self initWithServiceType:type name:@"" inDomain:domain];
    }
    return self;
}

- (id)initWithServiceType:(NSString *)type name:(NSString *)name inDomain:(NSString *)domain
{
    self = [super init];
    if (self)
    {
        self = [self initWithServiceType:type name:name inDomain:domain onPort:[self getPort]];
    }
    return self;
}

- (id)initWithServiceType:(NSString *)type name:(NSString *)name inDomain:(NSString *)domain onPort:(int)port
{
    self = [super init];
    if (self)
    {
        netServiceType = type;
        netServiceDomain = domain;
        netServiceTransportLayer = @"tcp";
        netServiceName = name;
        netServicePayloadSize = 128;
        netServicePort = port;
    }
    
    return self;
}

- (void)startBroadcasting
{
    NSLog(@"Start broadcasting");
    
    NSString *serviceType = [NSString stringWithFormat:@"_%@._%@.", netServiceType, netServiceTransportLayer];
    
    netService = [[NSNetService alloc] initWithDomain:netServiceDomain
                                                        type:serviceType
                                                        name:netServiceName
                                                        port:netServicePort];
    
    NSLog(@"Initialized with name %@, domain %@, type _%@._%@., on port %lu", netServiceName, netServiceDomain, netServiceType, netServiceTransportLayer, (long)netServicePort);
    
    [netService scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [netService publish];
    [netService setDelegate:self];
}

- (void)stop
{
    [self stopStream];
    [self stopService];
}

- (NSString *)getPairedDeviceName
{
    return [listeningService name];
}

- (void)sendData:(NSData *)data
{
    [outputStream write:(const uint8_t *)[data bytes] maxLength:[data length]];
}

#pragma mark - NSNetServiceDelegate

- (void)netServiceWillPublish:(NSNetService *)sender
{
    
}

- (void)netService:(NSNetService *)sender didNotPublish:(NSDictionary *)errorDict
{
    
}

- (void)netServiceDidPublish:(NSNetService *)sender
{
    NSLog(@"Publishing %@ of type %@ in domain %@ on port %li", [sender name], [sender type], [sender domain], (long)[sender port]);
}

- (void)netServiceWillResolve:(NSNetService *)sender
{
    
}

- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict
{
    
}

- (void)netServiceDidResolveAddress:(NSNetService *)sender
{
    listeningService = sender;
    
    if ([listeningService getInputStream:&inputStream outputStream:&outputStream])
    {
        [self connectInputStream:inputStream outputStream:outputStream];
    }

    [self.delegate didConnect];
}

- (void)netServiceDidStop:(NSNetService *)sender
{
    listeningService = nil;
    
    [self.delegate didDisconnect];
}

#pragma mark - NSStreamDelegate

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
    if (eventCode == NSStreamEventEndEncountered)
    {
        [self stopStream];
        
        [self.delegate didDisconnect];
    }
    else if (eventCode == NSStreamEventErrorOccurred)
    {
        [self stopService];
        [self stopStream];
        
        [self.delegate didDisconnect];
    }
    else if (eventCode == NSStreamEventHasBytesAvailable)
    {
        NSData *data = [self processData:aStream];
        
        if (data)
        {
            [self.delegate didReceiveData:data];
        }
    }
    else if (eventCode == NSStreamEventHasSpaceAvailable)
    {
        
    }
    else if (eventCode == NSStreamEventOpenCompleted)
    {
        // [self stopService];
        
        [self.delegate didConnect];
    }
    else if (eventCode == NSStreamEventNone)
    {
        
    }
}

#pragma mark - Helper Methods

- (NSData *)processData:(NSStream *)stream
{
    NSMutableData *data = [NSMutableData data];
    uint8_t *buffer = calloc(netServicePayloadSize, sizeof(uint8_t));
    NSUInteger length = 0;
    NSInputStream *iStream = (NSInputStream *)stream;
    
    while([iStream hasBytesAvailable])
    {
        length = [iStream read:buffer maxLength:netServicePayloadSize];
        if (length > 0) [data appendBytes:buffer length:length];
    }
    
    free(buffer);
    
    return data;
}

- (void)connectInputStream:(NSInputStream *)iStream outputStream:(NSOutputStream *)oStream
{
    inputStream = iStream;
    outputStream = oStream;
    
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [inputStream open];
    [outputStream open];
}

- (void)stopService
{
    if (netService)
    {
        [netService stop];
        [netService removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        netService = nil;
    }
}

- (void)stopStream
{
    if (inputStream)
    {
        [inputStream close];
        [inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        inputStream = nil;
    }
    if (outputStream)
    {
        [outputStream close];
        [outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        outputStream = nil;
    }
}

- (uint16_t)getPort
{
    uint16_t port;
    
    CFSocketContext socketContext = {0, (__bridge void *)(self), NULL, NULL, NULL};
    socket = CFSocketCreate(kCFAllocatorDefault, PF_INET, SOCK_STREAM, IPPROTO_TCP, kCFSocketAcceptCallBack, (CFSocketCallBack)&SocketAcceptedConnectionCallBack, &socketContext);
    
    if (!socket)
    {
        return 0;
    }
    
    int temp = 1;
    setsockopt(CFSocketGetNative(socket), SOL_SOCKET, SO_REUSEADDR, (void *)&temp, sizeof(temp));
    
    uint8_t payloadSize = netServicePayloadSize;
    setsockopt(CFSocketGetNative(socket), SOL_SOCKET, SO_SNDBUF, (void *)&payloadSize, sizeof(payloadSize));
    setsockopt(CFSocketGetNative(socket), SOL_SOCKET, SO_RCVBUF, (void *)&payloadSize, sizeof(payloadSize));
    
    struct sockaddr_in ipaddr4;
    memset(&ipaddr4, 0, sizeof(ipaddr4));
    ipaddr4.sin_len = sizeof(ipaddr4);
    ipaddr4.sin_family = AF_INET;
    ipaddr4.sin_port = 0;
    ipaddr4.sin_addr.s_addr = htonl(INADDR_ANY);
    
    NSData *ipaddress4 = [NSData dataWithBytes:&ipaddr4 length:sizeof(ipaddr4)];
    
    if (kCFSocketSuccess != CFSocketSetAddress(socket, (__bridge CFDataRef)ipaddress4))
    {
        if(socket) CFRelease(socket);
        socket = NULL;
        
        return 0;
    }
    else
    {
        NSData *address = (__bridge NSData *)CFSocketCopyAddress(socket);
        memcpy(&ipaddr4, [address bytes], [address length]);
        
        port = ntohs(ipaddr4.sin_port);
        
        CFRunLoopRef runLoop = CFRunLoopGetCurrent();
        CFRunLoopSourceRef source = CFSocketCreateRunLoopSource(kCFAllocatorDefault, socket, 0);
        CFRunLoopAddSource(runLoop, source, kCFRunLoopCommonModes);
        CFRelease(source);
    }
    
    return port;
}

static void SocketAcceptedConnectionCallBack(CFSocketRef socket, CFSocketCallBackType type, CFDataRef address, const void *data, void *info)
{
    if (kCFSocketAcceptCallBack == type)
    {
        XQExquisenseBonjourServer *server = (__bridge XQExquisenseBonjourServer *)info;
        
        CFSocketNativeHandle nativeSocketHandle = *(CFSocketNativeHandle *)data;
        CFReadStreamRef readStream = NULL;
		CFWriteStreamRef writeStream = NULL;
        CFStreamCreatePairWithSocket(kCFAllocatorDefault, nativeSocketHandle, &readStream, &writeStream);
        if(readStream && writeStream)
        {
            CFReadStreamSetProperty(readStream, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
            CFWriteStreamSetProperty(writeStream, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
            
            [server connectInputStream:(__bridge NSInputStream *)(readStream)
                          outputStream:(__bridge NSOutputStream *)(writeStream)];
        }
        else
        {
            close(nativeSocketHandle);
        }
        if (readStream) CFRelease(readStream);
        if (writeStream) CFRelease(writeStream);
    }
}

@end
